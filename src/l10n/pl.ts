<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="src/ui/about_dialog.ui" line="23"/>
        <source>About QSummer</source>
        <translation>O programie QSummer</translation>
    </message>
    <message>
        <location filename="src/ui/about_dialog.ui" line="58"/>
        <source>QSummer is a simple checksum validator.</source>
        <translation>QSummer jest prostym walidatorem sum kontrolnych.</translation>
    </message>
    <message>
        <location filename="src/ui/about_dialog.ui" line="71"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://orzechowski.tech/qsummer&quot;&gt;Homepage&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;a href=&quot;http://orzechowski.tech/qsummer&quot;&gt;Witryna programu&lt;/a&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="src/ui/about_dialog.ui" line="131"/>
        <source>&amp;Close</source>
        <translation>&amp;Zamknij</translation>
    </message>
    <message>
        <location filename="src/ui/about_dialog.ui" line="138"/>
        <source>Credits</source>
        <translation>Zasługi</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="src/ui/about_dialog.ui" line="87"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:small;&quot;&gt;Copyright ©2013-2017 Piotr Orzechowski [orzechowski.tech].&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:small;&quot;&gt;Copyright ©2013-2017 Piotr Orzechowski [orzechowski.tech].&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="src/ui/about_dialog.ui" line="103"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:small;&quot;&gt;This program comes with ABSOLUTELY NO WARRANTY;&lt;br/&gt;for details, visit &lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0.html&quot;&gt;http://www.gnu.org/licenses/gpl-3.0.html&lt;/a&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:small;&quot;&gt;Niniejszy program rozpowszechniany jest BEZ JAKIEJKOLWIEK GWARANCJI;&lt;br&gt;aby dowiedzieć się więcej, należy odwiedzić stronę &lt;a href=&quot;http://www.gnu.org/licenses/gpl-3.0.html&quot;&gt;http://www.gnu.org/licenses/gpl-3.0.html&lt;/a&gt;.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>CreditsDialog</name>
    <message>
        <location filename="src/ui/credits_dialog.ui" line="23"/>
        <source>Credits</source>
        <translation>Zasługi</translation>
    </message>
    <message>
        <location filename="src/ui/credits_dialog.ui" line="39"/>
        <source>Program</source>
        <translation>Program</translation>
    </message>
    <message>
        <location filename="src/ui/credits_dialog.ui" line="90"/>
        <source>Translations</source>
        <translation>Tłumaczenie</translation>
    </message>
    <message>
        <location filename="src/ui/credits_dialog.ui" line="168"/>
        <source>&amp;Close</source>
        <translation>&amp;Zamknij</translation>
    </message>
</context>
<context>
    <name>MainController</name>
    <message>
        <location filename="src/qsummer.py" line="280"/>
        <source>Checksums are equal</source>
        <translation>Sumy kontrolne są zgodne</translation>
    </message>
    <message>
        <location filename="src/qsummer.py" line="283"/>
        <source>Checksums ARE NOT equal</source>
        <translation>Sumy kontrolne NIE SĄ zgodne</translation>
    </message>
    <message>
        <location filename="src/qsummer.py" line="194"/>
        <source>(None)</source>
        <translation>(Brak)</translation>
    </message>
    <message>
        <location filename="src/qsummer.py" line="317"/>
        <source>&amp;Verify</source>
        <translation>&amp;Weryfikuj</translation>
    </message>
    <message>
        <location filename="src/qsummer.py" line="324"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Anuluj</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="src/ui/main_window.ui" line="107"/>
        <source>(None)</source>
        <translation>(Brak)</translation>
    </message>
    <message>
        <location filename="src/ui/main_window.ui" line="128"/>
        <source>&amp;Verify</source>
        <translation>&amp;Weryfikuj</translation>
    </message>
    <message>
        <location filename="src/ui/main_window.ui" line="163"/>
        <source>&amp;File</source>
        <translation>&amp;Plik</translation>
    </message>
    <message>
        <location filename="src/ui/main_window.ui" line="174"/>
        <source>&amp;Help</source>
        <translation>P&amp;omoc</translation>
    </message>
    <message>
        <location filename="src/ui/main_window.ui" line="189"/>
        <source>New</source>
        <translation>Nowy</translation>
    </message>
    <message>
        <location filename="src/ui/main_window.ui" line="192"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="src/ui/main_window.ui" line="202"/>
        <source>Quit</source>
        <translation>Zakończ</translation>
    </message>
    <message>
        <location filename="src/ui/main_window.ui" line="205"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="src/ui/main_window.ui" line="215"/>
        <source>About</source>
        <translation>O programie</translation>
    </message>
</context>
</TS>
