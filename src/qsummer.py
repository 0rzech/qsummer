#!/usr/bin/python3

'''
Copyright (C)2013-2017 Piotr Orzechowski [orzechowski.tech].

This file is part of QSummer.

QSummer is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 3 of the License.

QSummer is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with QSummer; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

@author: Piotr Orzechowski [orzechowski.tech]
'''

import argparse
import locale
import sys

from os import path
from os.path import abspath
from os.path import basename
from os.path import dirname
from os.path import join

from hashlib import md5
from hashlib import sha1
from hashlib import sha224
from hashlib import sha256
from hashlib import sha384
from hashlib import sha512

from PySide import QtCore
from PySide import QtGui

from PySide.QtCore import QFile
from PySide.QtCore import QObject
from PySide.QtCore import QMutex
from PySide.QtCore import Qt
from PySide.QtCore import QThread

from PySide.QtGui import QAction
from PySide.QtGui import QApplication
from PySide.QtGui import QComboBox
from PySide.QtGui import QDialogButtonBox
from PySide.QtGui import QIcon
from PySide.QtGui import QLabel
from PySide.QtGui import QLineEdit
from PySide.QtGui import QMainWindow
from PySide.QtGui import QProgressBar
from PySide.QtGui import QPushButton
from PySide.QtGui import QSpacerItem
from PySide.QtGui import QStatusBar

from PySide.QtUiTools import QUiLoader

ROOT_DIR = abspath(dirname(__file__))


class ChecksumWorker(QObject):

    fractionCalculated = QtCore.Signal(int)
    calculationFinished = QtCore.Signal(str)
    calculationCancelled = QtCore.Signal()
    calculationException = QtCore.Signal(object)

    def __init__(self, file_name, method):
        QObject.__init__(self)

        self._fileName = file_name
        self._method = method
        self._mutex = QMutex()
        self._stopped = False

    @QtCore.Slot()
    def run(self):
        try:
            with open(self._fileName, 'rb') as file:
                fileSize = path.getsize(self._fileName)
                chunkSize = fileSize // 99

                bytesRead = file.read(chunkSize)
                totalBytesRead = len(bytesRead)

                while bytesRead:
                    self._mutex.lock()
                    if self._stopped:
                        self._mutex.unlock()
                        self.calculationCancelled.emit()
                        return
                    self._mutex.unlock()

                    self._method.update(bytesRead)
                    bytesRead = file.read(chunkSize)
                    totalBytesRead += len(bytesRead)

                    self.fractionCalculated.emit(totalBytesRead / fileSize * 100)

            return self.calculationFinished.emit(self._method.hexdigest())

        except Exception as e:
            self.calculationException.emit(e)

    def stop(self):
        self._mutex.lock()
        self._stopped = True
        self._mutex.unlock()


class MainController(QObject):

    def __init__(self, fileName=''):
        QObject.__init__(self)

        self._thread = QThread()
        self._worker = None
        self._fileName = fileName

        loader = QUiLoader()

        self.loadMainWindow(loader)
        self.loadAboutDialog(loader)
        self.loadCreditsDialog(loader)

        self.setupUi()
        self.setupConnections()

    def loadMainWindow(self, loader):
        self._window = loader.load(join(ROOT_DIR, 'ui/main_window.ui'))
        self._window.setWindowFlags(Qt.WindowCloseButtonHint | Qt.WindowMinimizeButtonHint)

        self._fileChooser = self._window.findChild(QPushButton, 'fileChooser')
        self._comboBox = self._window.findChild(QComboBox, 'comboBox')
        self._lineEdit = self._window.findChild(QLineEdit, 'lineEdit')
        self._progressBar = self._window.findChild(QProgressBar, 'progressBar')
        self._verifyCancelButton = self._window.findChild(QPushButton, 'verifyCancelButton')

        self._statusBar = QLabel()
        self._window.findChild(QStatusBar, 'statusBar').addPermanentWidget(self._statusBar, 1)
        self._statusBar.setAlignment(Qt.AlignHCenter)

        self._newAction = self._window.findChild(QAction, 'actionNew')
        self._quitAction = self._window.findChild(QAction, 'actionQuit')
        self._aboutAction = self._window.findChild(QAction, 'actionAbout')

        if len(self._fileName):
            self._fileChooser.setText(basename(self._fileName))
            self._verifyCancelButton.setEnabled(True)

    def loadAboutDialog(self, loader):
        self._aboutDialog = loader.load(join(ROOT_DIR, 'ui/about_dialog.ui'))

    def loadCreditsDialog(self, loader):
        self._creditsDialog = loader.load(join(ROOT_DIR, 'ui/credits_dialog.ui'))

    def setupUi(self):
        self._comboBox.addItem('MD5')
        self._comboBox.addItem('SHA-1')
        self._comboBox.addItem('SHA-224')
        self._comboBox.addItem('SHA-256')
        self._comboBox.addItem('SHA-348')
        self._comboBox.addItem('SHA-512')
        self._comboBox.setCurrentIndex(3)

    def setupConnections(self):
        self._newAction.activated.connect(self.reset)
        self._fileChooser.clicked.connect(self.showFileChooserDialog)
        self._aboutAction.activated.connect(self.showAboutDialog)
        self._verifyCancelButton.clicked.connect(self.startVerification)
        button = self._aboutDialog.findChild(QPushButton, 'creditsButton')
        button.clicked.connect(self.showCreditsDialog)

    def show(self):
        self._window.show()

    def reset(self):
        self._verifyCancelButton.setEnabled(False)
        self._fileChooser.setText(self.tr('(None)'))
        self._lineEdit.setText('')
        self._progressBar.setValue(0)
        self._statusBar.setText('')

    def showFileChooserDialog(self):
        self._fileName, _ = QtGui.QFileDialog.getOpenFileName()
        if len(self._fileName):
            self._fileChooser.setText(basename(self._fileName))
            self._verifyCancelButton.setEnabled(True)

    def showAboutDialog(self):
        self._aboutDialog.show()

    def showCreditsDialog(self):
        self._creditsDialog.show()

    def startVerification(self):
        self.setCancelAction()

        self._progressBar.setValue(0)
        self._fileChooser.setEnabled(False)
        self._comboBox.setEnabled(False)
        self._lineEdit.setEnabled(False)
        self._newAction.setEnabled(False)
        self._statusBar.setText('')

        self._startThread()

    def _startThread(self):
        self._worker = ChecksumWorker(self._fileName, self.getAlgorithm())
        self._worker.moveToThread(self._thread)

        self._thread.started.connect(self._worker.run)
        self._worker.calculationFinished.connect(self.onCalculationFinished)
        self._worker.fractionCalculated.connect(self.onFractionCalculated)
        self._worker.calculationCancelled.connect(self.onCalculationCancelled)
        self._worker.calculationException.connect(self.onCalculationException)

        self._thread.start()

    def cancelVerification(self):
        self.setVerifyAction()

        self._worker.stop()
        self._thread.quit()
        self._worker = None

        self._fileChooser.setEnabled(True)
        self._comboBox.setEnabled(True)
        self._lineEdit.setEnabled(True)
        self._newAction.setEnabled(True)

    def getAlgorithm(self):
        options = {
            0: md5(),
            1: sha1(),
            2: sha224(),
            3: sha256(),
            4: sha384(),
            5: sha512()
        }
        return options[self._comboBox.currentIndex()]

    @QtCore.Slot(int)
    def onFractionCalculated(self, fraction):
        self._progressBar.setValue(fraction)

    @QtCore.Slot(str)
    def onCalculationFinished(self, checksum):
        self._thread.quit()
        self._worker = None

        self.setVerifyAction()

        if checksum == self._lineEdit.text():
            self._statusBar.setStyleSheet('QLabel { color: darkgreen; }')
            self._statusBar.setText(self.tr('Checksums are equal'))
        else:
            self._statusBar.setStyleSheet('QLabel { color: red; }')
            self._statusBar.setText(self.tr('Checksums ARE NOT equal'))

        self._progressBar.setValue(100)
        self._fileChooser.setEnabled(True)
        self._comboBox.setEnabled(True)
        self._lineEdit.setEnabled(True)
        self._newAction.setEnabled(True)

    @QtCore.Slot()
    def onCalculationCancelled(self):
        self._progressBar.setValue(0)

    @QtCore.Slot(object)
    def onCalculationException(self, exception):
        self._thread.quit()
        self._worker = None

        self._statusBar.setStyleSheet('QLabel { color: red; }')
        self._statusBar.setText(str(exception))

        self.setVerifyAction()

        self._progressBar.setValue(100)
        self._fileChooser.setEnabled(True)
        self._comboBox.setEnabled(True)
        self._lineEdit.setEnabled(True)
        self._newAction.setEnabled(True)

    def setVerifyAction(self):
        self._verifyCancelButton.clicked.disconnect(self.cancelVerification)
        self._verifyCancelButton.clicked.connect(self.startVerification)
        self._verifyCancelButton.setText(self.tr('&Verify'))
        self._verifyCancelButton.setIcon(QIcon.fromTheme('media-playback-start'))

    def setCancelAction(self):
        self._verifyCancelButton.clicked.disconnect(self.startVerification)
        self._verifyCancelButton.clicked.connect(self.cancelVerification)
        self._verifyCancelButton.setText(self.tr('&Cancel'))
        self._verifyCancelButton.setIcon(QIcon.fromTheme('process-stop'))

    def quit(self, *args):
        if self._worker is not None:
            self._worker.stop()
            self._thread.quit()
            self._thread.wait()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='QSummer is a simple checksum validator.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-f', '--file', type=str, nargs='?', const='', default='',
                        help='set file to check', dest='file')
    args = parser.parse_args()

    translator = QtCore.QTranslator()
    translator.load(join(ROOT_DIR, 'l10n/' + locale.getlocale()[0] + '.qm'))

    app = QApplication(sys.argv)
    app.installTranslator(translator)

    controller = MainController(args.file)
    app.aboutToQuit.connect(controller.quit)
    controller.show()

    sys.exit(app.exec_())
