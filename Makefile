src = src
dst = dist

i18n:
	pyside-lupdate qsummer.pro

l10n: i18n
	mkdir -p ${dst}/l10n
	
	lrelease ${src}/l10n/cs.ts -qm ${dst}/l10n/cs.qm
	lrelease ${src}/l10n/pl.ts -qm ${dst}/l10n/pl.qm

build: l10n
	mkdir -p ${dst}/ui
	
	cp ${src}/ui/*.ui ${dst}/ui
	cp ${src}/qsummer.py ${dst}/qsummer
	
	chmod +x ${dst}/qsummer

rebuild: clean build

clean:
	rm -rf ${dst}

run:
	${dst}/qsummer
