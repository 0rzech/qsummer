# QSummer - Simple checksum validator

[What is QSummer](#what-is-qsummer) | [Features](#features) | [Screenshots](#screenshots) |
[Install](#install) | [License](LICENSE) | [Changelog](doc/changelog.md)

## What is QSummer?

QSummer is a simple graphical application used to validate file checksums.<br>
Just run QSummer, choose file to validate, select hash algorithm, paste checksum provided by the
file creator and hit "Validate".<br>
Once the validation is finished, you will know whether file is ok or damaged.

## Features

 * Support for md5, sha1, sha224, sha256, sha384 and sha512 hash algorithms
 * Cross-platform - should be able to run anywhere Bash, Python 3 and PySide 1.1.0+ are available 
   (though icons will be shown on systems adhering to freedesktop.org
   [Icon Naming Specification](http://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html)
   only)
 * Multilingual (currently Czech, English and Polish languages are available)
 * Easy to use - has simple graphical interface

## Screenshots

![new.png](doc/screenshots/new.png)<br>
_QSummer is started or "File->New" was chosen from menu_

![running.png](doc/screenshots/running.png)<br>
_QSummer is calculating checksum_

![success.png](doc/screenshots/success.png)<br>
_Calculated checksum matched the pasted one_

![fail.png](doc/screenshots/fail.png)<br>
_Calculated checksum did not match the pasted one_

## Install

#### Requirements

1. Build
 * Make
 * Python 3
 * pyside-lupdate from pyside-tools package
 * lrelease from Qt's Linguist tool chain

2. Run
 * Python 3
 * PySide 1.1.0+ (Python 3 version)

#### Installation

Basically there's no need to install QSummer (although lack of installation script makes its version
0.9.4 rather than 1.0) and thus the installation procedure is as follows:

1. Download sources from `releases` tab.
2. Go to project directory.
3. Run `make build`.
4. Rename `dist` directory to your liking and copy it wherever you like.
5. You're done!

#### Usage

You can start the application by issuing the command `application_directory/qsummer` on the command
line.

To preload QSummer with file to check, you have to pass it the file name parameter on the command
line, eg. `qsummer -f file_to_check`.

You can also create `.desktop` file to run the application with your desktop environment.
