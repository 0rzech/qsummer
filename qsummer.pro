SOURCES = src/qsummer.py
FORMS = src/ui/main_window.ui \
    src/ui/about_dialog.ui \
    src/ui/credits_dialog.ui
TRANSLATIONS = src/l10n/cs.ts \
    src/l10n/pl.ts
