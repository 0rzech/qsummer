#### Version 0.9.4 (2017-04-08 CEST)
 * Add '-f' '--file' command line parameter
 * Remove the necessity to use additional bash script

#### Version 0.9.3 (2017-04-03 CEST)
 * Add file command line parameter handling
 * Select SHA-256 by default

#### Version 0.9.2 (2017-04-02 CEST)
 * Add Makefile
 * Update licensing and contact information

#### Version 0.9.1 (2013-06-23 CEST)
 * Add Czech translation; thanks to Pavel Fric \[[fripohled.blogspot.cz](http://fripohled.blogspot.cz)\]
 * Fix minor bugs
